# Data directory
This is a collection of scripts and manipulation classes to interact with SIM-TIRF data of the specification defined in [`./data_structure.html`](./data_structure.html).
