#! python3

import numpy as np
import matplotlib.pyplot as plt
import os

directory_path = "."
os.mkdir("pngs")
for filename in os.listdir(directory_path):
    if ".tif" in filename or ".tiff" in filename:
        plt.imsave(
            directory_path + "/" + "pngs/" + filename.strip(".tif") + ".png",
            np.uint8(plt.imread(directory_path + "/" + filename)),
        )
