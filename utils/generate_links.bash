#! /bin/env bash

# make folders - raw data
for file in ./raw_data/*/*/* ./raw_data/*/*/*/*; do
    # if it is not a in the `./data` directory
    if [[ ! -d "$(dirname $(echo $file | sed s/\\.\\/raw_data\\//\\.\\/data\\//))" ]]; then
        # make the directory with all its parent directories
        mkdir --parents "$(dirname $(echo $file | sed s/\\.\\/raw_data\\//\\.\\/data\\//))"
    fi
done

# make folders - dynamic data
for file in ./dynamic_data/*/*/* ; do
    # if it is not a in the `./data` directory
    if [[ ! -d "$(dirname $(echo $file | sed s/\\.\\/dynamic_data\\//\\.\\/data\\//))" ]]; then
        # make the directory with all its parent directories
        mkdir --parents "$(dirname $(echo $file | sed s/\\.\\/dynamic_data\\//\\.\\/data\\//))"
    fi
done


# link files - raw data
for file in ./raw_data/*/*/* ./raw_data/*/*/*/*; do
    # if it is not a directory
    if [[ ! -d "$file" ]]; then
        # if the link does not already exist
        if [[ ! -f "$(echo $file | sed s/\\.\\/raw_data\\//\\.\\/data\\//)" ]]; then
            # make the link in the directory `./data`
            ln --symbolic --relative --target-directory="$(dirname $(echo $file | sed s/\\.\\/raw_data\\//\\.\\/data\\//))" $file
        fi
    fi
done

# link files - dynamic data
for file in ./dynamic_data/*/*/*; do
    # if it is not a directory
    if [[ ! -d "$file" ]]; then
        # if the link does not already exist
        if [[ ! -f "$(echo $file | sed s/\\.\\/dynamic_data\\//\\.\\/data\\//)" ]]; then
            # make the link in the directory `./data`
            ln --symbolic --relative --target-directory="$(dirname $(echo $file | sed s/\\.\\/dynamic_data\\//\\.\\/data\\//))" $file
        fi
    fi
done
