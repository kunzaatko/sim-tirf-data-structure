#! /bin/env zsh

INPUT_FILE=$1

if [ -f $INPUT_FILE ]; then
    BASEDIR=$(dirname $INPUT_FILE)
    FILENAME=$(basename $INPUT_FILE)
    BASENAME=$(echo $FILENAME | cut -f 1 -d '.')
    EXTENTION=$(echo $FILENAME | cut -f 2 -d '.')

    if [[ $(echo $EXTENTION | tr '[:upper:]' '[:lower:]') != "tif" && $(echo $EXTENTION | tr '[:upper:]' '[:lower:]') != "tiff" ]]
    then
        echo "$INPUT_FILE is not a 'TIFF' image"
    fi

else
    echo "$INPUT_FILE is not a file"
fi

NUMBER_OF_FRAMES=$(tiffdump $INPUT_FILE | grep -c '^Directory')

# the file exists and is of the right format

# create extracting directory
if [ ! -d $BASEDIR/$BASENAME ]
then
    mkdir $BASEDIR/$BASENAME
fi

convert $INPUT_FILE $BASEDIR/$BASENAME/"$BASENAME"-%d.tif

NUMBER_OF_TAIL_DIGITS=${#NUMBER_OF_FRAMES}
NUMBER_OF_SORT_FIELDS=$(echo "$BASEDIR/$BASENAME/$BASENAME-" | grep -o "-" | grep -c "")
FRAMES=( $( ls $BASEDIR/$BASENAME/$BASENAME-*.tif | sort --field-separator=- --numeric-sort --key=$(echo $(( $NUMBER_OF_SORT_FIELDS+1 ))),$(echo $(( $NUMBER_OF_SORT_FIELDS+1 )) ) ) )
for tail in $(seq -w 0 $(( ${NUMBER_OF_FRAMES}-1 )) ); do
    if [[ "$FRAMES[$(( $tail + 1 ))]" != "$BASEDIR/$BASENAME/${BASENAME}-${tail}.tif" ]]
        then
            mv $FRAMES[$(( $tail + 1 ))] $BASEDIR/$BASENAME/${BASENAME}-${tail}.tif;
    fi
done
