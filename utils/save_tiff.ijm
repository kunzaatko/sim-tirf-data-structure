id = getArgument;
run("Bio-Formats", "open=" + id + " " +
  "autoscale color_mode=Default view=Hyperstack stack_order=XYCZT");
saveAs("Tiff", File.directory + File.nameWithoutExtension + ".tiff");
run("Close")
run("Quit")
